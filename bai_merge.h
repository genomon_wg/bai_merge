#ifndef BAI_MERGE_H
#define BAI_MERGE_H

#ifdef VERBOSE
#define PRINT_MSG(...)  fprintf(stderr, __VA_ARGS__)
#else
#define PRINT_MSG(...)  ((void)0)  
#endif

#define BGZF_TERMINAL_BLOCK_SIZE    28

#define BGZF_HEADER_LEN (12+6)

#define PSEUD_BIN   37450


void check_endian(void);

void copy_and_shift_bai_data(const char *bai_file_name, FILE *fout, off_t shift, int last);


#endif /* BAI_MERGE_H */
