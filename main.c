#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

#include "bai_merge.h"

static off_t get_file_size(const char *name)
{
    struct stat stbuf;
    if (stat(name, &stbuf) != 0) {
        perror(name);
        exit(1);
    }
    return stbuf.st_size;
}


static inline uint16_t unpack_uint16(const uint8_t *buf)
{
    return (uint16_t)buf[0] | (uint16_t)buf[1] << 8;
}


static off_t get_1st_block_size(const char *name)
{
    FILE *fp;
    uint8_t bgzf_header[BGZF_HEADER_LEN];
    if ((fp = fopen(name, "r")) == NULL) {
        perror(name);
        exit(1);
    }
    int n_read = fread(bgzf_header, 1, BGZF_HEADER_LEN, fp);
    fclose(fp);
	assert(n_read == BGZF_HEADER_LEN);
    return unpack_uint16(&bgzf_header[16]) + 1;
}


char **read_bam_file_list(const char *file_name, int *n_file)
{
    FILE *fp;
    char buf[1048];
    int n = 0, m = 0;
    char **list = NULL;

    if ((fp = fopen(file_name, "r")) == NULL) {
        perror(file_name);
        exit(1);
    }
    while (fgets(buf, sizeof(buf), fp)) {
        int len = strlen(buf);
        if (buf[len-1] == '\n') buf[--len] = '\0';
        if (len == 0) continue;
        if (buf[0] == '#') continue;
        if (n == m) {
            m = m ? m << 1 : 128;
            list = (char**)realloc(list, m * sizeof(char*));
        }
        list[n++] = strdup(buf);
    }
    fclose(fp);

    *n_file = n;
    return list;
}


int main(int argc, char *argv[])
{
    if (argc < 2 || (strcmp(argv[1], "-b") == 0 && argc != 3)) {
        fprintf(stderr, "%s ref1.bam ref2.bam ... refN.bam\n", argv[0]);
        fprintf(stderr, "%s -b bam_file_list\n", argv[0]);
        exit(1);
    }

    int n_file;
    char **bam_file_list;
    if (strcmp(argv[1], "-b") == 0) {
        bam_file_list = read_bam_file_list(argv[2], &n_file);
    } else {
        n_file = argc - 1;
        bam_file_list = &argv[1];
    }
    fprintf(stderr, "n_file = %d\n", n_file);

    check_endian();

    off_t shift = 0;

    for (int i_file = 0; i_file < n_file; i_file++) {
        fprintf(stderr, "file %d\n", i_file);

        const char *bam_file = bam_file_list[i_file];
        char *bai_file = malloc(strlen(bam_file) + 5);
        strcpy(bai_file, bam_file);
        strcpy(&bai_file[strlen(bam_file)], ".bai");
        fprintf(stderr, "    bam: %s\n", bam_file);
        fprintf(stderr, "    bai: %s\n", bai_file);

        off_t file_size = get_file_size(bam_file);
        off_t header_block_size = get_1st_block_size(bam_file);
        fprintf(stderr, "    bam file size: %lu\n", file_size);
        fprintf(stderr, "    header block size: %lu\n", header_block_size);

        if (i_file > 0) shift -= header_block_size;

        fprintf(stderr, "    shift BGZF block: %lu\n", shift);

        int last = (i_file == n_file -1) ? 1 : 0;
        copy_and_shift_bai_data(bai_file, stdout, shift, last);

        shift += file_size - BGZF_TERMINAL_BLOCK_SIZE;
    }

    return 0;
}
