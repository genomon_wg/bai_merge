#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "bai_merge.h"


static int big_endian = 0;

void check_endian(void)
{
    int i = 1;
    if (*((char *)&i + (sizeof(int) - 1))) big_endian = 1;
}


static inline void swap32(void *p)
{
    uint32_t *u = p;
    *u = ((*u << 24) & 0xff000000) | ((*u <<  8) & 0x00ff0000)
       | ((*u >>  8) & 0x0000ff00) | ((*u >> 24) & 0x000000ff);
}

static inline void swap64(void *p)
{
    uint64_t *u = p;
    *u = ((*u << 56) & 0xff00000000000000)
       | ((*u << 40) & 0x00ff000000000000)
       | ((*u << 24) & 0x0000ff0000000000)
       | ((*u <<  8) & 0x000000ff00000000)
       | ((*u >>  8) & 0x00000000ff000000)
       | ((*u >> 24) & 0x0000000000ff0000)
       | ((*u >> 40) & 0x000000000000ff00)
       | ((*u >> 56) & 0x00000000000000ff);
}


static inline uint32_t read_uint32(FILE *fp)
{
    uint32_t v;
    int n_read = fread(&v, 4, 1, fp);
    assert(n_read == 1);
    if (big_endian) swap32(&v);
    return v;
}

static inline void write_uint32(FILE *fp, uint32_t v)
{
    if (big_endian) swap32(&v);
    int n_write = fwrite(&v, 4, 1, fp);
    assert(n_write == 1);
}


static inline int32_t read_int32(FILE *fp)
{
    int32_t v;
    int n_read = fread(&v, 4, 1, fp);
    assert(n_read == 1);
    if (big_endian) swap32(&v);
    return v;
}

static inline void write_int32(FILE *fp, int32_t v)
{
    if (big_endian) swap32(&v);
    int n_write = fwrite(&v, 4, 1, fp);
    assert(n_write == 1);
}


static inline uint64_t read_uint64(FILE *fp)
{
    uint64_t v;
    int n_read = fread(&v, 8, 1, fp);
    assert(n_read == 1);
    if (big_endian) swap64(&v);
    return v;
}

static inline void write_uint64(FILE *fp, uint64_t v)
{
    if (big_endian) swap64(&v);
    int n_write = fwrite(&v, 8, 1, fp);
    assert(n_write == 1);
}


/* 
 * virtual file offset
 */
typedef struct {
    uint64_t coffset;  /* use only lower 48 bits */
    uint16_t uoffset;
} vf_offset_t;

static inline vf_offset_t read_vf_offset(FILE *fp)
{
    uint64_t tmp = read_uint64(fp);
    vf_offset_t v;
    v.uoffset = tmp & 0xffff;
    v.coffset = tmp >> 16;
    return v;
}

static inline void write_vf_offset(FILE *fp, vf_offset_t v)
{
    uint64_t tmp = v.coffset << 16 | v.uoffset;
    write_uint64(fp, tmp);
}

static void shift_and_write_vf_offset(FILE *fp, vf_offset_t v, off_t shift)
{
    v.coffset += shift;
    write_vf_offset(fp, v);
}


static int first = 1;
static int prev_ref = -1;

void copy_and_shift_bai_data(const char *bai_file_name, FILE *fout, off_t shift, int last)
{
    FILE *fin = fopen(bai_file_name, "r");
    if (fin == NULL) {
        if (errno == ENOENT) {
            fprintf(stderr, "***warn: bai file does not exist\n");
            return;
        }
        perror(bai_file_name);
        exit(1);
    }

    char magic[4];
    int n_read = fread(magic, 1, 4, fin);
    assert(n_read == 4);
    assert(strncmp(magic, "BAI\1", 4) == 0);

    int n_ref = read_int32(fin);

    if (first) {
        /* 1st bai */
        int n_write = fwrite(magic, 1, 4, fout);
        assert(n_write == 4);
        write_int32(fout, n_ref);
        first = 0;
    }

    int current_ref = -1;
    for (int i_ref = 0; i_ref < n_ref; i_ref++) {
        int n_bin = read_int32(fin);
        if (n_bin == 0) {
            int n_intv = read_int32(fin);
            assert(n_intv == 0);
            continue;
        }

        if (last) fprintf(stderr, "***warn: last bai file is not RNAME '*'\n");

        if (current_ref == -1) {
            current_ref = i_ref;
            if (current_ref <= prev_ref) {
                fprintf(stderr, "***error: bai file list maybe invalidly orderd");
                exit(1);
            }
        } else {
            fprintf(stderr, "***error: %s includes more than one reference data.", bai_file_name);
            exit(1);
        }

        for (int j_ref = prev_ref + 1; j_ref < current_ref; j_ref++) {
            /* fill "n_bin = 0, n_intv = 0" */
            write_int32(fout, 0);
            write_int32(fout, 0);
        }

        fprintf(stderr, "    ref: %d\n", current_ref);
        write_int32(fout, n_bin);
        PRINT_MSG("        n_bin =  %d\n", n_bin);

        for (int i_bin = 0; i_bin < n_bin; i_bin++) {
            uint32_t bin = read_uint32(fin);    
            write_uint32(fout, bin);
            int n_chunk = read_int32(fin);
            write_int32(fout, n_chunk);
            if (bin != PSEUD_BIN) {
                PRINT_MSG("        bin %u\n", bin);
                PRINT_MSG("            n_chunk = %d\n", n_chunk);
                for (int i_chunk = 0; i_chunk < n_chunk; i_chunk++) {
                    vf_offset_t chunk_beg = read_vf_offset(fin);
                    vf_offset_t chunk_end = read_vf_offset(fin);
                    shift_and_write_vf_offset(fout, chunk_beg, shift);
                    shift_and_write_vf_offset(fout, chunk_end, shift);
                    PRINT_MSG("            %lu:%u - %lu:%u\n",
                              chunk_beg.coffset, chunk_beg.uoffset,
                              chunk_end.coffset, chunk_end.uoffset);
                }
            } else {
                /* pseud-bin */
                PRINT_MSG("        bin %u (pseud-bin)\n", bin);
                assert(n_chunk == 2);
                vf_offset_t unmapped_beg = read_vf_offset(fin);
                vf_offset_t unmapped_end = read_vf_offset(fin);
                shift_and_write_vf_offset(fout, unmapped_beg, shift);
                shift_and_write_vf_offset(fout, unmapped_end, shift);
                uint64_t n_mapped = read_uint64(fin);
                uint64_t n_unmapped = read_uint64(fin);
                write_uint64(fout, n_mapped);
                write_uint64(fout, n_unmapped);
                PRINT_MSG("            unmapped reads %lu:%u - %lu:%u\n",
                          unmapped_beg.coffset, unmapped_beg.uoffset,
                          unmapped_end.coffset, unmapped_end.uoffset);
                PRINT_MSG("            # of mapped read-segments = %lu\n", n_mapped);
                PRINT_MSG("            # of unmapped read-segments = %lu\n", n_unmapped);
            }
        }

        int n_intv = read_int32(fin);
        write_int32(fout, n_intv);
        PRINT_MSG("        n_intv = %d\n", n_intv);
        for (int i_intv = 0; i_intv < n_intv; i_intv++) {
            vf_offset_t ioffset = read_vf_offset(fin);
            shift_and_write_vf_offset(fout, ioffset, shift);
            PRINT_MSG("            %lu:%u\n", ioffset.coffset, ioffset.uoffset);
        }
    }

    if (current_ref > -1) prev_ref = current_ref;

    if (last) {
        if (current_ref == -1) fprintf(stderr, "    ref: %d\n", n_ref);
        current_ref = n_ref;
        for (int j_ref = prev_ref + 1; j_ref < current_ref; j_ref++) {
            /* fill "n_bin = 0, n_intv = 0" */
            write_int32(fout, 0);
            write_int32(fout, 0);
        }
    }

    /* optional n_no_coor field */
    uint64_t n_no_coor;
    n_read = fread(&n_no_coor, 8, 1, fin);
    if (n_read == 1) {
        if (big_endian) swap64(&n_no_coor);
        if (n_no_coor > 0 && current_ref != n_ref) {
            fprintf(stderr, "***error: although ref != '*', n_no_coor > 0\n"); 
            fprintf(stderr, "          ref = %d, n_no_coor = %lu\n", current_ref, n_no_coor);
            exit(1);
        }
        if (current_ref == n_ref) {
            write_uint64(fout, n_no_coor);
            PRINT_MSG("        n_no_coor = %lu\n", n_no_coor);
        }
    }

    fclose(fin);
}
