# bai_merge

BAMインデックスファイル(*.bam.bai)のマージプログラム。

## Usage

    bai_merge ref1.bam ref2.bam ... refN.bam > merged.bam.bai

または、

    bai_merge -b bam_file_list > merged.bam.bai

BAMファイルのリスト、または、BAMファイルのリストを記述したファイルを指定する。
BAMインデックスファイルのリストではないことに注意。

## 制限

BAMファイルは、リファレンス配列単位で分割されている必要がある。

コマンドラインでは、リファレンス配列の定義順(SAMファイルヘッダに記述された順)にBAMファイルを指定する。

各BAMファイルの先頭BGZFブロックには、BAMヘッダ情報のみで、アライメント情報が含まれていないことを仮定している。
samtools view -b および bammarkduplicatesの生成するBAMファイルは、この条件を満たしている。